from graphics import *

def main():
    ancho=600
    largo=300
    win = GraphWin("Stop", ancho, largo)

    message=Text(Point(100, 20),'Nombre')
    message.draw(win)
    textoNombre = Entry(Point(100, 50), 10)
    textoNombre.setFill('white')
    textoNombre.draw(win)

    message = Text(Point(200, 20), 'Ciudad')
    message.draw(win)
    textoCiudad = Entry(Point(200, 50), 10)
    textoCiudad.setFill('white')
    textoCiudad.draw(win)

    message = Text(Point(300, 20), 'Animal')
    message.draw(win)
    textoAnimal = Entry(Point(300, 50), 10)
    textoAnimal.setFill('white')
    textoAnimal.draw(win)

    message = Text(Point(400, 20), 'Cosa')
    message.draw(win)
    textoCosa = Entry(Point(400, 50), 10)
    textoCosa.setFill('white')
    textoCosa.draw(win)

    botonFin=Rectangle(Point((ancho/2)-50,150), Point((ancho/2)+50,200))
    botonFin.draw(win)


    leerArchivo('nombres.txt')
    win.getMouse() # Pause to view result
    win.close()    # Close window when done



def leerArchivo(nombre):
    f=open(nombre, 'r')
    mensaje=f.read()
    print(mensaje)
    f.close()


main()